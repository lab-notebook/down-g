# downG

[![License: CC0-1.0](https://licensebuttons.net/l/zero/1.0/80x15.png)](http://creativecommons.org/publicdomain/zero/1.0/)

Download genomes and annotations from NCBI


## Requirements
NodeJS-14

One way to install nodeJS is using NVM:
https://github.com/nvm-sh/nvm
## Install

```
git clone https://gitlab.com/lab-notebook/down-g.git
cd down-g
npm install -g
```

## Use
```
down-g filename genomeType fileType
```

where:
    `filename` is the name of the file with an NCBI identifier per line.
    `genomeType` is the desired type of genome [genomic/protein]
    `fileType` is the desired file type [.fna/.faa/.gff/.gpff], dependent on genomeType

Example filename:

```
GCF_001601075.1
GCF_001652685.1
GCF_900007885.1
GCF_004010155.1
GCF_001601535.1
```

## Uninstall
go to the directory of the package and:
```
npm uninstall -g
```
