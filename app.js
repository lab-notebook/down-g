#!/usr/bin/env node
const fs = require('fs');
const ncbi = require('bionode-ncbi');
const axios = require('axios');

const idList = fs.readFileSync(process.argv[2]).toString().split('\n')

// Genome and file type
var genType = process.argv[3]
var fileType = process.argv[4]

const downOne = (id) => {
    return new Promise((resolve, reject) => {
        ncbi.urls('assembly', id)
            .on('data', (response) => {
                const url = response[genType][fileType]
                console.log(url)
                axios({
                        method: "get",
                        url,
                        responseType: "stream"
                    }).then(function (response) {
                        response.data.pipe(fs.createWriteStream(`${id}.${fileType}.gz`))
                            .on('finish', () => {
                            console.log('Done')
                            resolve()
                            })
                            .on('error', (err) => {
                                console.error(err)
                                reject()
                            });
                        });
            });
    });
}


const run = async () => {
    for (id of idList) {
        console.log(`Downloading ${id}`)
        await downOne(id)
    }
    console.log('All set')
}

run();